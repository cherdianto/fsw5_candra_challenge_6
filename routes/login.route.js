const express = require('express');
const router = express.Router();
const db = require('../models/index');

router.post("/", (req, res) => {
    const {username, password} = req.body;
    db.User.findAll({
        where: {
            username: username,
            password: password
        },
        attributes: [
            "id",
            "username",
            "password"
        ]
    })
        .then((user) => {
            user = user.map(i => i.get({ plain: true}))[0];
            if (user.username== 'admin') {
                // console.log('halo');
                res.redirect('/users/dashboard');
            } else {
                res.render('game', {user})
            }
        })
        .catch((err) => {
            // res.status(400);
            res.redirect('/register');
        })
});

module.exports = router;
