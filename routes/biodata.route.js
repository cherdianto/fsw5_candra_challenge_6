const express = require('express');
const router = express.Router();
const db = require('../models/index');
// const user = require('./models/user');

router.get("/", (req, res) => {
    db.Biodata.findAll()
        .then((biodata) => {
            res.status(200).json(biodata)
        })
        .catch((err) => {
            res.status(400).json({
                message: err.message
            });
        })
});

router.get('/add/:id', (req, res) => {
    var id = req.params.id;
    console.log(id);
    res.render('biodata',{id});
})

router.get("/:id", (req, res) => {
    db.Biodata.findAll({
        where: {
            user_id: req.params.id
        }
    })
        .then((biodata) => {
            biodata = biodata.map(i => i.get({ plain: true}))[0];
            var id = req.params.id;
            if (biodata == null)
                res.render('biodata', {id})
            else 
                res.render('biodata-edit', {biodata})
        })
        .catch((err) => {
            res.status(400).json({
                message: err.message
            });
        })
})

router.post('/add', (req, res) => {
    console.log(req.body);
    const {
        user_id,
        name,
        email
    } = req.body;
    db.Biodata.create({
            user_id,
            name,
            email
        })
        .then((biodata) => {
            res.redirect('/biodata')
        })
        .catch(err => {
            res.status(400).json("cannot register, error: " + err.message);
        })
})

router.delete('/:id', (req, res) => {
    db.Biodata.destroy({
            where: {
                id: req.params.id
            }
        })
        .then((biodata) => {
            res.status(201).json(biodata);
            // res.redirect('/articles')
        })
        .catch(err => {
            res.status(400).json("cannot delete the data, error: " + err.message);
        })
})

router.put('/:id', (req, res) => {
    console.log('masuk sini');
    const {
        user_id,
        name,
        email
    } = req.body;
    db.Biodata.update(req.body, {
            where: {
                user_id: req.params.id
            }
        })
        .then((biodata) => {
            res.redirect('/users/dashboard')
        })
        .catch(err => {
            res.status(400).json("cannot update the data, error: " + err.message);
        })
})

module.exports = router;
