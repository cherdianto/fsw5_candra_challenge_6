const express = require('express');
const router = express.Router();
const db = require('../models/index');

router.get("/", (req, res) => {
    db.Game_history.findAll({
        include: {
            model: db.User
        },
    })
        .then((game_history) => {
            res.render('history',{game_history})
        })
        .catch((err) => {
            res.status(400).json({
                message: err.message
            });
        })
});

router.get("/:id", (req, res) => {
    db.Game_history.findAll({
        where:{
            game_id: req.params.id
        }
    })
        .then((game_history) => {
            res.render('history', {game_history})
        })
        .catch((err) => {
            res.status(400).json({
                message: err.message
            });
        })
})

router.post('/', (req, res) => {
    console.log('req body :' + req.body);
    const {
        game_id,
        login,
        status
    } = req.body;
    db.Game_history.create({
            game_id,
            login,
            status
        })
        .then((game_history) => {
            // res.redirect('/history')
            res.status(200);

        })
        .catch(err => {
            res.status(400).json(err.message);
        })
})

// router.get("/:id", (req, res) => {
//     db.Game_history.findByPk(req.params.id)
//         .then((game_history) => {
//             res.status(200).json(game_history)
//         })
//         .catch((err) => {
//             res.status(400).json({
//                 message: err.message
//             });
//         })
// })



router.delete('/:id', (req, res) => {
    db.Game_history.destroy({
            where: {
                id: req.params.id
            }
        })
        .then((game_history) => {
            // res.status(201).json(game_history);
            res.redirect('/history')
        })
        .catch(err => {
            res.status(400).json("cannot delete the data, error: " + err.message);
        })
})

module.exports = router;