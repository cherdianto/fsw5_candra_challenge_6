const express = require('express');
const router = express.Router();
const db = require('../models/index');
// const user = require('./models/user');

router.get("/dashboard", (req, res) => {
    db.User.findAll({
        // include : [{all: true}]
            include: [
                // {
                //     model: db.Game_history
                // },
                { 
                    model: db.Biodata
                },
            ]
        })
        .then((users) => {
            // res.status(200).json(users)
            // users = users.map(i => i.get({ plain: true}))[0];
            // console.log(users);
            res.render('users', {
                users
            });
        })
        .catch((err) => {
            res.status(400).json({
                message: err.message
            });
        })
});


router.get("/:id", (req, res) => {
    db.User.findByPk(req.params.id)
        .then((user) => {
            res.status(200).json(user)
        })
        .catch((err) => {
            res.status(400).json({
                message: err.message
            });
        })
})

router.put('/:id', (req, res) => {
    console.log('masuk sini');
    const {
        username,
        password
    } = req.body;
    db.User.update(req.body, {
            where: {
                id: req.params.id
            }
        })
        .then((user) => {
            res.redirect('/users')
        })
        .catch(err => {
            res.status(400).json("cannot update the data, error: " + err.message);
        })
})

router.delete('/:id', (req, res) => {
    // console.log('masuk sini');
    db.User.destroy({
            where: {
                id: req.params.id
            }
        })
        .then((user) => {
            // res.status(201).json(user);
            res.redirect('/users/dashboard')
        })
        .catch(err => {
            res.status(400).json("cannot delete the data, error: " + err.message);
        })
})

router.post('/register', (req, res) => {
    const {
        username,
        password
    } = req.body;
    db.User.create({
            username,
            password
        })
        .then((user) => {
            // console.log(typeof user);
            // res.status(200).json(user.username);
            res.redirect('/users')
        })
        .catch(err => {
            res.status(400).json("cannot register, error: " + err.message);
        })
})

module.exports = router;