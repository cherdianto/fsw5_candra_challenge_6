const express = require('express');
const router = express.Router();
const db = require('../models/index');



router.get('/', (req, res) => {
    res.render('home');
})

router.get('/game', (req, res) => {
    // res.render('game');
    res.redirect('/login');
})

router.get('/login', (req, res) => {
    res.render('login');
})

router.get('/register', (req, res) => {
    res.render('register');
})

router.get('/users', (req, res) => {
    // res.render('login');
    res.redirect('/login');
})


module.exports = router;