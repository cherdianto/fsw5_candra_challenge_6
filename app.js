const { urlencoded } = require('express');
const morgan = require('morgan');
const express = require('express');
const methodOverride = require('method-override')
const app = express();
const fs = require('fs');
const { runInNewContext } = require('vm');
const path = require('path');
const { resourceUsage } = require('process');
// let userDb = require('./masterdata/user.json');
const port = 3000;
const db = require('./models/index');


//middleware
app.use(express.json());
app.use(urlencoded({extended : false}));
app.use(morgan('dev'));
app.use(express.static(path.join(__dirname, "public")));
app.use(methodOverride('_method'));

app.set('view engine', 'ejs');

//NEW ROUTING
console.log(path.join(__dirname, "public"));
const userRoutes = require("./routes/user.route");
const pageRoutes = require("./routes/page.route");
const biodataRoutes = require("./routes/biodata.route");
const game_historyRoutes = require("./routes/game_history.route");
const loginRoutes = require("./routes/login.route");

app.use('/', pageRoutes);
app.use('/users', userRoutes);
app.use('/biodata', biodataRoutes);
app.use('/history', game_historyRoutes);
app.use('/login', loginRoutes);

// LOGIN 



// =============== ERROR HANDLING ===============
// ***************** INTERNAL ERROR *************
app.use((err, req, res, next) => {
    var error = err.message;
    res.status(500).render('internalError', {error});
})
// ***************** 404 ERROR ******************
app.use((req, res, next) => {
    res.render('404');
})

app.listen(port, () => {
    console.log('listening on port ' + port)
})



