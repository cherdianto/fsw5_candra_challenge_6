## running the sequelize :
1. sequelize db:create
2. sequelize db:migrate
3. sequelize db:seed:all
it will create 3 usernames, biodatas, and game histories

## super admin username
username : admin
password : admin

## create new user :
1. go to localhost:3000/register ( or click the 'signup' link in the navbar )

## login as regular user
1. make sure you have an account
2. go to localhost:3000/login ( or click the 'login' link in the navbar )
3. now you can play the game, your game record will be available through administrator panel

