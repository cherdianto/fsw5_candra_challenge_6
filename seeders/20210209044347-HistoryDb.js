'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Game_histories', 
    [
      {
        game_id: 2,
        login: new Date(),
        status: "win",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        game_id: 2,
        login: new Date(),
        status: "lose",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        game_id: 2,
        login: new Date(),
        status: "draw",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        game_id: 3,
        login: new Date(),
        status: "win",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        game_id: 3,
        login: new Date(),
        status: "win",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        game_id: 3,
        login: new Date(),
        status: "win",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        game_id: 3,
        login: new Date(),
        status: "lose",
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Game_histories', null, {});
  }
};
