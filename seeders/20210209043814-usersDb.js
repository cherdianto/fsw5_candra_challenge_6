'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Users', 
    [
      {
        username: 'admin',
        password: 'admin',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'cherdianto',
        password: '123456',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'rnisa',
        password: '123456',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Users', null, {});
  }
};
