'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Biodata', 
    [
      {
        user_id: 1,
        name: 'administrator',
        email: "admin@gmial.com",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 2,
        name: 'Candra Herdianto',
        email: "ch@gmial.com",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        user_id: 3,
        name: 'R. Nisa',
        email: "rn@gmial.com",
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('Biodata', null, {});
  }
};
