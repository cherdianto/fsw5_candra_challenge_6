'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Game_history extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Game_history.belongsTo(models.User, {
        foreignKey: {
          name: 'game_id',
          as:'User'
        }
      })
    }
  };
  Game_history.init({
    game_id: DataTypes.INTEGER,
    login: DataTypes.STRING,
    status: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Game_history',
  });
  return Game_history;
};