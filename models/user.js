'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      User.hasMany(models.Game_history,{
          foreignKey: {
             name: 'game_id',
             as:'game_history'
          }
        })

      User.hasMany(models.Biodata, {
        foreignKey: {
          name: 'user_id',
          as: 'biodata'
        }
      })
    }
  };
  User.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};